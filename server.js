'use strict';
// Bug here
const express = require('express');
const os = require('os');
const uuid = require('uuid/v4');
const pino = require('pino');
const pinoExpress = require('express-pino-logger');
// run npm i <package name>
const logger = pino({
  level: process.env.LOG_LEVEL || 
  'info'
});
const loggerExpress = pinoExpress(logger);
var PropertiesReader = require('properties-reader');
var isAlive = true;

// Constants
const PORT = process.env.PORT || 8080;
const HOST = '0.0.0.0';
const hostname = os.hostname();
const config = process.env.CONFIG || 'config/app.ini';
const properties = PropertiesReader(config);
const version = PropertiesReader('config/version.ini').get('main.version');
const BACKEND_URL = process.env.BACKEND_URL || `http://localhost:${PORT}/version`
logger.info('BACKEND URL: ' + BACKEND_URL);

// App
const app = express();
// set log
app.use(loggerExpress);

// Main Function
app.get('/', (req, res) => {
  if (!isAlive)
    res.status(504).send(
      `Backend version:${version},Response:504, Message: Backend is stopped`
    );
  else {
    var protocol = 'http'
    if (BACKEND_URL.startsWith('https')) protocol = 'https';
      var https = require(protocol);
      var client = https.get(BACKEND_URL, (resp) => {
      logger.info(`${BACKEND_URL} return ${resp.statusCode}`);
      res.status(resp.statusCode).send(
        `Backend version:${version},Response:${resp.statusCode},Host:${hostname}, Message: ${properties.get('main.message')}`
      );
    });
    client.on('error', error => {
      logger.error(error);
      res.status(503).send(
        `Backend version:${version},Response:503,Host:${hostname},Message: ${error}`
      );
    });
    client.end();
  }
});

app.get('/stop', (req, res) => {
  isAlive = false;
  res.status(200).send(
    `Backend version:${version},Response:200,Message: ${hostname} is stopped`
  );
  logger.info('App is stopped working');
});



app.get('/start', (req, res) => {
  isAlive = true;
  res.status(200).send(
    `Backend version:${version},Response:200,Message: ${hostname} is started`
  );
  logger.info('App is started');
});

app.get('/status', (req, res) => {
  var status = 200;
  var message = 'running';
  if (isAlive == false) {
    status = 503;
    message = 'unavailable';
    logger.info('App status = Not Ready');
  } else {
    logger.info('App status = Ready');
  }
  res.status(status).send(
    `Backend version:${version},Response:${status},${hostname} is ${message}`
  );
});

app.get('/version', (req, res) => {
  logger.info("Check version");
  res.status(200).send(
    `Backend version:${version},Response:200`
  );
});

app.listen(PORT, HOST);
logger.info(`Running on http://${HOST}:${PORT}`);
// To support npm run version
// add following line under scripts
// "version": "echo $npm_package_version",
